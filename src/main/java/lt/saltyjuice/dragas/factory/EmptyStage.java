package lt.saltyjuice.dragas.factory;

import lt.saltyjuice.dragas.factory.exception.InconsumableException;

/**
 * An empty stage that returns input. Intended to use
 * with validating rules, since they only test input
 */
public class EmptyStage implements Stage {
    /**
     * Default empty stage.
     *
     * @see EmptyStage
     */
    public static final Stage DEFAULT = new EmptyStage();

    private EmptyStage() {

    }

    @Override
    public Object apply(Object input) throws InconsumableException {
        return input;
    }
}
