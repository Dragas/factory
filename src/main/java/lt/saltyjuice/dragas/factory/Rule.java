package lt.saltyjuice.dragas.factory;

import lt.saltyjuice.dragas.factory.exception.InconsumableException;

/**
 * Rule is an interface which is supposed to test the provided object
 * against some condition. If the said rule passes, dependents may
 * call {@link Rule#getStage(Object)} and apply that stage on the provided
 * input object
 */
public interface Rule {

    /**
     * Returns a stage that is capable of consuming input object
     *
     * @param input an object that is supposed to be consumed
     * @return Stage that can consume this object
     * @throws InconsumableException when there is no stage that can consume input
     */
    Stage getStage(Object input) throws InconsumableException;
}
