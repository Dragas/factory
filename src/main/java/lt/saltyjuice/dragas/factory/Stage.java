package lt.saltyjuice.dragas.factory;

import lt.saltyjuice.dragas.factory.exception.InconsumableException;

/**
 * Stage is a function that is applied to input object to produce some sort
 * of output.
 */
public interface Stage {

    /**
     * Consumes an input object to produce some sort of output
     *
     * @param input Object to be consumed
     * @return Some output that should be used in the next stage
     * @throws InconsumableException when this stage cannot consume provided object
     */
    Object apply(Object input) throws InconsumableException;
}
