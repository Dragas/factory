package lt.saltyjuice.dragas.factory;

import lt.saltyjuice.dragas.factory.exception.InconsumableException;

/**
 * An empty rule, that always returns provided stage.
 */
public class EmptyRule implements Rule {

    private final Stage stage;

    public EmptyRule(Stage stage) {
        this.stage = stage;
    }

    @Override
    public Stage getStage(Object input) throws InconsumableException {
        return stage;
    }
}
