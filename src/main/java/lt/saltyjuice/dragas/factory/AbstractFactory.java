package lt.saltyjuice.dragas.factory;

import lt.saltyjuice.dragas.factory.exception.InconsumableException;

import java.util.ArrayList;
import java.util.List;

/**
 * Factory implementations are encouraged to
 * Use this class instead instead of direct factory
 * as it tends to have same construct.
 *
 * It is up to implementations to choose how to evaluate rules.
 * Some might decide to apply ALL rules that can consume
 * the input object, while some may only want to apply first
 * one.
 */
abstract public class AbstractFactory implements Factory {
    protected List<Rule> rules;

    public AbstractFactory() {
        rules = new ArrayList<>();
    }

    @Override
    public Factory addRule(Rule rule) {
        // Some security to prevent atleast basic circles in graph
        if (!rules.contains(rule) && rule != this)
            rules.add(rule);
        return this;
    }

    /**
     * By default, factories return themselves as stages. This
     * may be scope breaking but at the same time, it fixes
     * an issue where factories are supposed to return a stage.
     *
     * @param input an object that is supposed to be consumed
     * @return this
     * @throws InconsumableException Doesn't really throw this unless implementations
     *                               redefined it.
     */
    @Override
    public Stage getStage(Object input) throws InconsumableException {
        return this;
    }
}
