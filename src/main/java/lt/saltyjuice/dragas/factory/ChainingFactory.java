package lt.saltyjuice.dragas.factory;

import lt.saltyjuice.dragas.factory.exception.InconsumableException;

/**
 * Chaining factories are such factories that
 * push result of previous rule into the next.
 * If one link fails - entire factory fails.
 * <p>
 * You can consider them as "all match" factories with a twist.
 */
public class ChainingFactory extends AbstractFactory {
    @Override
    public Object apply(Object input) throws InconsumableException {
        for (Rule rule : rules) {
            input = rule.getStage(input).apply(input);
        }
        return input;
    }
}
