package lt.saltyjuice.dragas.factory;

/**
 * Pipeline is a set of stages that are each applied
 * to input object to determine what should be output.
 * <p>
 * Pipelines may be used in place of stages, to build
 * more complex object consumers.
 *
 * Since Pipelines extend Stage interface, rules may opt to
 * return multiple stages that could each be applied to
 * the input element.
 *
 * @deprecated since factories already implement stage interface, this is kinda moot
 */
public interface Pipeline extends Stage {

}
