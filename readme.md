# Factory

Conditional code execution according to predefined rules.

Currently the module is only intended to contain interfaces 
which will be implemented in their own modules.

Provides 3 interfaces and 1 exception, to build a factory that is
capable of consuming objects of particular type.

In addition, several helper classes are provided to help
kickstart module's usage.

## Workflow

Intended workflow is as follows:

* Implement `Rule` interface to return `Stage` implementation
* Group the rules with `Factory` implementations via `Factory#addRule`.
* Add other factories to root factory if necessary for hierarchy
* Pass the input object via `Factory#apply` method
  * If factory succeeds, you ought to return intended result object.
  * Otherwise a Inconsumable exception should be thrown.
  
It is up to implementations to decide whether or not they want to
create new objects, mutate input or return the same object.

## Example Intended usage

```java
Factory root = new ChainingFactory()
    .addRule(new ValidatingRule())
    .addRule(new GeneratingFactory())
    .addRule(new FormattingFactory());
root.apply("FOO BAR"); // should output something reasonable
```
  
  
## Notes
 
* It might be reasonable to dive into generic hell
